define(['zepto'], function ($) {
    var lastTimer = null;
    var isMouseOut = true;
    var isHide = true;
    $('.g-social .container .card').forEach(function (cardEl) {
        var $cardEl = $(cardEl);
        var $accountEl = $cardEl.find('.account-id');
        var $bonusEl = $cardEl.find('.bonus-wrapper > .bonus');
        $accountEl.on('click', function () {
            if(isHide) {
                $cardEl.find('.bonus-wrapper > .normal').css('display', 'none');
                $bonusEl.css('display', 'inline-block');
                isHide = false;
            } else {
                clearTimeout(lastTimer);
            }
            lastTimer = setTimeout(function () {
                $cardEl.find('.bonus-wrapper > .normal').css('display', 'inline-block');
                $bonusEl.css('display', 'none');
                isHide = true;
            }, 1000);
        });
        $bonusEl.on('click', function () {
            var selection = window.getSelection();
            var range = document.createRange();
            range.selectNodeContents($accountEl[0]);
            selection.removeAllRanges();            
            selection.addRange(range);
            document.execCommand('copy');
            $bonusEl.find('.clickme').text('Copied');
            $bonusEl.css({
                backgroundColor: '#4C5375',
                color: '#FFF'
            });
            setTimeout(function () {
                $bonusEl.css({
                    backgroundColor: '',
                    color: ''
                });                
                $bonusEl.find('.clickme').text('Click ME');
                if (isMouseOut) {
                    $cardEl.find('.bonus-wrapper > .normal').css('display', 'inline-block');
                    $bonusEl.css('display', 'none');
                    isHide = true;
                }
            }, 1000);
        });
        $bonusEl.on('mouseover', function () {
            isMouseOut = false;
            clearTimeout(lastTimer);
        });
        $bonusEl.on('mouseout', function () {
            isMouseOut = true;
            lastTimer = setTimeout(function () {
                $cardEl.find('.bonus-wrapper > .normal').css('display', 'inline-block');
                $bonusEl.css('display', 'none');
                isHide = true;
            }, 1000);
        });
    });
    return {};
});