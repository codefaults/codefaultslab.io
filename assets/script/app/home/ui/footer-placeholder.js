define(['zepto', 'app/utils'], function($, util) {
    var footer_wrapper = $('.g-footer-wrapper')[0];
    var placeholder = document.createElement('div');
    placeholder.style = "display:block;height:" + util.getComputedBox(footer_wrapper).computedHeight + "px;width:100%;visibility:hidden;";
    placeholder.className = 'g-footer-placeholder';
    footer_wrapper.parentElement.insertBefore(placeholder, footer_wrapper);
    window.addEventListener('resize', function () {
        setTimeout(function () {
            placeholder.style.height = util.getComputedBox(footer_wrapper).computedHeight + 'px';
        }, 0);
    });
    return {};
});