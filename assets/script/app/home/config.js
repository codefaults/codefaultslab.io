'use strict';
console.log('require.js loaded');

require.config({
    baseUrl: "assets/script/app/home",
    paths: {
        app: '.',
        mod: 'modules',
        ui: 'ui',
        ux: 'ux',
        zepto: '/assets/script/lib/zepto.min'
    }
});

requirejs(['app/main']);