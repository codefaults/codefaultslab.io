define(function() {
    function getcomputedbox(el) {
        var computedStyle = el.currentStyle || window.getComputedStyle(el);
        var margin = {
            top: parseFloat(computedStyle.marginTop) || 0,
            right: parseFloat(computedStyle.marginRight) || 0,
            bottom: parseFloat(computedStyle.marginBottom) || 0,
            left: parseFloat(computedStyle.marginLeft) || 0
        };
        var padding = {
            top: parseFloat(computedStyle.paddingTop) || 0,
            right: parseFloat(computedStyle.paddingRight) || 0,
            bottom: parseFloat(computedStyle.paddingBottom) || 0,
            left: parseFloat(computedStyle.paddingLeft) || 0
        };
        var border = {
            top: parseFloat(computedStyle.borderTopWidth) || 0,
            right: parseFloat(computedStyle.borderRightWidth) || 0,
            bottom: parseFloat(computedStyle.borderBottomWidth) || 0,
            left: parseFloat(computedStyle.borderLeftWidth) || 0
        };
        var width = el.offsetWidth;
        var height = el.offsetHeight;
        return {
            margin: margin,
            padding: padding,
            border: border,
            width: width,
            height: height,
            boxWidth: padding.left + padding.right + border.left + border.right + width,
            boxHeight: padding.top + padding.bottom + border.top + border.bottom + height,
            computedWidth: margin.left + margin.right + padding.left + padding.right + border.left + border.right + width,
            computedHeight: margin.top + margin.bottom + padding.top + padding.bottom + border.top + border.bottom + height
        };
    }
    return {
        getComputedBox: getcomputedbox
    };
})